export type User = {
  balance: string;
  picture: string;
  age: number;
  eyeColor: string;
  name: UserName;
  company: string;
  email: string;
  address: string;
}

export type UserName = {
  first: string;
  last: string;
}
