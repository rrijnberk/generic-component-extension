import {User} from "../../models/user.model";

export const USER_DATA: User[] = [
  {
    "balance": "$1,011.68",
    "picture": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQdojgKGd6m4ZMuj2xl9MmydcNNtQdOA0OY6LmKeEkPAU8BRrt5",
    "age": 27,
    "eyeColor": "brown",
    "name": {
      "first": "Angelita",
      "last": "Buckner"
    },
    "company": "BIFLEX",
    "email": "angelita.buckner@biflex.me",
    "address": "199 Rockaway Avenue, Wildwood, Louisiana, 8929"
  },
  {
    "balance": "$3,464.17",
    "picture": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5saV35FJkzl9DdviWCwK8YwMSOn9su6Ly4NgaVxCffRSP--rsOw",
    "age": 38,
    "eyeColor": "green",
    "name": {
      "first": "Payne",
      "last": "Rodgers"
    },
    "company": "SOLGAN",
    "email": "payne.rodgers@solgan.tv",
    "address": "412 Amber Street, Brutus, Georgia, 3342"
  },
  {
    "balance": "$2,701.88",
    "picture": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStnzD_1Kwaa-kCujwCt2vZr0W0nTtc-4NXJxGOHJEhzV0gr6mS",
    "age": 26,
    "eyeColor": "green",
    "name": {
      "first": "Ashley",
      "last": "Craig"
    },
    "company": "PHOTOBIN",
    "email": "ashley.craig@photobin.us",
    "address": "115 Halleck Street, Baker, Maryland, 7239"
  },
  {
    "balance": "$2,586.45",
    "picture": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-vF4mxy-ry8-R0BUO-mbWedfZSw-n1LgjjbctEcnm9TCAcZf6",
    "age": 40,
    "eyeColor": "green",
    "name": {
      "first": "Melendez",
      "last": "Yates"
    },
    "company": "BEDLAM",
    "email": "melendez.yates@bedlam.biz",
    "address": "322 Vandervoort Place, Chamberino, New York, 7142"
  },
  {
    "balance": "$3,550.47",
    "picture": "http://placehold.it/32x32",
    "age": 35,
    "eyeColor": "blue",
    "name": {
      "first": "Colleen",
      "last": "Hodge"
    },
    "company": "SARASONIC",
    "email": "colleen.hodge@sarasonic.name",
    "address": "709 Canton Court, Thomasville, Arizona, 1208"
  },
  {
    "balance": "$3,936.97",
    "picture": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzkJ_JpNPw9ujvpNLXOeUzPkDRGidmaNQgQo5RTJU9sYqRu34H1Q",
    "age": 22,
    "eyeColor": "blue",
    "name": {
      "first": "Mcdonald",
      "last": "Bentley"
    },
    "company": "ZILLACTIC",
    "email": "mcdonald.bentley@zillactic.net",
    "address": "944 Saratoga Avenue, Selma, Nebraska, 5064"
  },
  {
    "balance": "$3,487.54",
    "picture": "http://placehold.it/32x32",
    "age": 35,
    "eyeColor": "brown",
    "name": {
      "first": "Kathleen",
      "last": "Dennis"
    },
    "company": "TECHADE",
    "email": "kathleen.dennis@techade.info",
    "address": "570 Pleasant Place, Marenisco, Florida, 1892"
  },
  {
    "balance": "$3,111.44",
    "picture": "http://placehold.it/32x32",
    "age": 27,
    "eyeColor": "green",
    "name": {
      "first": "Robbins",
      "last": "Luna"
    },
    "company": "OMATOM",
    "email": "robbins.luna@omatom.com",
    "address": "314 Harman Street, Canterwood, South Carolina, 1012"
  },
  {
    "balance": "$3,688.42",
    "picture": "http://placehold.it/32x32",
    "age": 27,
    "eyeColor": "blue",
    "name": {
      "first": "Kinney",
      "last": "Jarvis"
    },
    "company": "ECLIPTO",
    "email": "kinney.jarvis@eclipto.org",
    "address": "617 Ide Court, Sperryville, Colorado, 8925"
  },
  {
    "balance": "$2,849.71",
    "picture": "http://placehold.it/32x32",
    "age": 33,
    "eyeColor": "green",
    "name": {
      "first": "Darlene",
      "last": "Garcia"
    },
    "company": "RAMJOB",
    "email": "darlene.garcia@ramjob.biz",
    "address": "904 Kings Place, Logan, Wisconsin, 357"
  },
  {
    "balance": "$2,800.65",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "brown",
    "name": {
      "first": "Whitfield",
      "last": "Morgan"
    },
    "company": "QOT",
    "email": "whitfield.morgan@qot.io",
    "address": "102 Kansas Place, Juarez, Hawaii, 1946"
  },
  {
    "balance": "$3,232.98",
    "picture": "http://placehold.it/32x32",
    "age": 39,
    "eyeColor": "green",
    "name": {
      "first": "Estela",
      "last": "Webster"
    },
    "company": "ZYTRAC",
    "email": "estela.webster@zytrac.ca",
    "address": "986 McKinley Avenue, Freelandville, North Carolina, 1716"
  },
  {
    "balance": "$1,104.93",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "green",
    "name": {
      "first": "Le",
      "last": "Oneil"
    },
    "company": "OVERFORK",
    "email": "le.oneil@overfork.me",
    "address": "231 Truxton Street, Strong, New Mexico, 5004"
  },
  {
    "balance": "$3,712.62",
    "picture": "http://placehold.it/32x32",
    "age": 34,
    "eyeColor": "blue",
    "name": {
      "first": "Berger",
      "last": "Alvarado"
    },
    "company": "EBIDCO",
    "email": "berger.alvarado@ebidco.tv",
    "address": "554 Flatlands Avenue, Ventress, Michigan, 4847"
  },
  {
    "balance": "$2,844.75",
    "picture": "http://placehold.it/32x32",
    "age": 22,
    "eyeColor": "green",
    "name": {
      "first": "Delores",
      "last": "Hayden"
    },
    "company": "AVENETRO",
    "email": "delores.hayden@avenetro.us",
    "address": "594 Vine Street, Shaft, Idaho, 7597"
  },
  {
    "balance": "$1,730.92",
    "picture": "http://placehold.it/32x32",
    "age": 40,
    "eyeColor": "green",
    "name": {
      "first": "Middleton",
      "last": "Hendrix"
    },
    "company": "ZENTIME",
    "email": "middleton.hendrix@zentime.biz",
    "address": "491 Sedgwick Place, Taycheedah, Virginia, 2417"
  },
  {
    "balance": "$2,362.20",
    "picture": "http://placehold.it/32x32",
    "age": 34,
    "eyeColor": "blue",
    "name": {
      "first": "Eddie",
      "last": "Christian"
    },
    "company": "CALCULA",
    "email": "eddie.christian@calcula.name",
    "address": "353 Whitty Lane, Snelling, Maine, 4647"
  },
  {
    "balance": "$3,887.99",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "brown",
    "name": {
      "first": "Gallagher",
      "last": "Rasmussen"
    },
    "company": "PROVIDCO",
    "email": "gallagher.rasmussen@providco.net",
    "address": "833 Gatling Place, Keyport, Puerto Rico, 8108"
  },
  {
    "balance": "$3,308.39",
    "picture": "http://placehold.it/32x32",
    "age": 22,
    "eyeColor": "green",
    "name": {
      "first": "Elva",
      "last": "Barrera"
    },
    "company": "NETAGY",
    "email": "elva.barrera@netagy.info",
    "address": "173 Monroe Place, Carbonville, Alabama, 1530"
  },
  {
    "balance": "$1,477.62",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "eyeColor": "blue",
    "name": {
      "first": "Amelia",
      "last": "Bartlett"
    },
    "company": "INTERGEEK",
    "email": "amelia.bartlett@intergeek.com",
    "address": "133 Williamsburg Street, Fillmore, West Virginia, 6269"
  },
  {
    "balance": "$3,390.29",
    "picture": "http://placehold.it/32x32",
    "age": 20,
    "eyeColor": "blue",
    "name": {
      "first": "Reyna",
      "last": "Ellis"
    },
    "company": "GRAINSPOT",
    "email": "reyna.ellis@grainspot.org",
    "address": "547 Kenilworth Place, Crown, Iowa, 7504"
  },
  {
    "balance": "$1,528.02",
    "picture": "http://placehold.it/32x32",
    "age": 31,
    "eyeColor": "brown",
    "name": {
      "first": "Dorthy",
      "last": "Vinson"
    },
    "company": "NEUROCELL",
    "email": "dorthy.vinson@neurocell.biz",
    "address": "909 Burnett Street, Barclay, Wyoming, 3695"
  },
  {
    "balance": "$1,705.74",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "green",
    "name": {
      "first": "Sanford",
      "last": "Tyson"
    },
    "company": "CYCLONICA",
    "email": "sanford.tyson@cyclonica.io",
    "address": "879 Noel Avenue, Balm, Montana, 5415"
  },
  {
    "balance": "$1,876.85",
    "picture": "http://placehold.it/32x32",
    "age": 36,
    "eyeColor": "brown",
    "name": {
      "first": "Yates",
      "last": "Manning"
    },
    "company": "COMVEY",
    "email": "yates.manning@comvey.ca",
    "address": "737 Brighton Court, Fivepointville, New Hampshire, 3854"
  },
  {
    "balance": "$2,983.97",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "brown",
    "name": {
      "first": "Mccarthy",
      "last": "Dickerson"
    },
    "company": "BALOOBA",
    "email": "mccarthy.dickerson@balooba.me",
    "address": "908 Chester Avenue, Seymour, Illinois, 3162"
  },
  {
    "balance": "$2,283.46",
    "picture": "http://placehold.it/32x32",
    "age": 20,
    "eyeColor": "blue",
    "name": {
      "first": "Bradshaw",
      "last": "Fuentes"
    },
    "company": "IMPERIUM",
    "email": "bradshaw.fuentes@imperium.tv",
    "address": "318 Marconi Place, Sheatown, South Dakota, 892"
  },
  {
    "balance": "$3,957.81",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "brown",
    "name": {
      "first": "Lavonne",
      "last": "Johns"
    },
    "company": "PODUNK",
    "email": "lavonne.johns@podunk.us",
    "address": "476 Hinsdale Street, Curtice, Massachusetts, 6799"
  }
]
