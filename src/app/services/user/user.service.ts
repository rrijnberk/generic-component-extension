import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {USER_DATA} from "./user-data.mock";
import {User} from "../../models/user.model";

@Injectable()
export class UserService {
  get(): Observable<User[]> {
    return of(USER_DATA)
  }
}
