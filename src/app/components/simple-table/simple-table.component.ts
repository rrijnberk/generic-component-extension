import {Component, ContentChild, Input, OnInit, TemplateRef} from '@angular/core'

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html'
})
export class SimpleTableComponent implements OnInit {
  @Input() cols;
  @Input() values;

  @ContentChild('header', {
    static: true
  })
  @Input('header') headerTemplate: TemplateRef<any>;


  @ContentChild('body', {
    static: true
  })
  @Input('body')
  bodyTemplate: TemplateRef<any>;




  ngOnInit(): void {

  }


}
