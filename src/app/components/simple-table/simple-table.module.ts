import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {UserService} from "../../services/user/user.service";
import {SimpleTableComponent} from "./simple-table.component";
import {TableModule} from "primeng/table";

@NgModule({
  declarations: [
    SimpleTableComponent
  ],
  imports: [
    BrowserModule,
    TableModule
  ],
  providers: [
    UserService
  ],
  exports: [
    SimpleTableComponent
  ]
})
export class SimpleTableModule { }
