import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {User} from "../../models/user.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  users: User[];
  cols: any[];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.get().subscribe(users => this.users = users);

    this.cols = [
      { field: 'picture', header: 'picture', type: 'image' },
      { field: 'company', header: 'company' },
      { field: 'name', header: 'name', type: 'name' },
      { field: 'email', header: 'email' },
      { field: 'age', header: 'age' },
      { field: 'eyeColor', header: 'eyeColor' },
      { field: 'balance', header: 'balance' }
    ];
  }
}
